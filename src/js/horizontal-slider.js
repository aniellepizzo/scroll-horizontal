window.addEventListener("load", function(event) {    
    const year = $("#year p"),
    closeElements = $('.close'),
    openBoxElements = $('.open-page');

    let hash = window.location.hash ? window.location.hash.substring(1) : "2000";
    let page = $('.page');
    let transition = $('.transition-slide');

    //monta o slide principal
    let swiper = new Swiper('.horizontal-slider', {
        slidesPerView: 1.25,
        centeredSlides: true,
        paginationClickable: true,
        spaceBetween: 30,
        speed: 600,
        mousewheel: true,
        hashNavigation: true,
        allowSlidePrev: true,
        allowSlideNext: true,
        watchSlidesProgress: true,
        watchSlidesVisibility: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet: function (index, className) {                
                return '<span class="' + className + '">' + (years[index])  + '</span>';               
            },
        }
    });

    //NAVEGACAO PELO SLIDER
    $('.menu .2000').click(function (e) {
        e.preventDefault();
        swiper.slideTo(0, 0);
    });

    $('.menu .2001').click(function (e) {
        e.preventDefault();
        swiper.slideTo(2, 0);
    });

    $('.menu .2002').click(function (e) {
        e.preventDefault();
        swiper.slideTo(4, 0);
    });

    $('.menu .2003').click(function (e) {
        e.preventDefault();
        swiper.slideTo(6, 0);
    });

    //CLASSE DE ACTIVE MENU NAVEGACAO SLIDER
    let btnYear = $('.menu li'),
        indexBtn = btnYear.attr("data-index");

    btnYear.click(function(){
        $(this).addClass('active')
        $(this).siblings('li').removeClass('active')
    })    
  
    swiper.on('slideChange', function () {
      var slide = swiper.activeIndex;
  
      btnYear.each(function(){
          var $selfAttr = $(this).attr('data-index');
          
          if( $selfAttr == slide || $selfAttr <= slide){
            $(this).addClass('active');
            $(this).siblings('li').removeClass('active');
          }
      })
    });

    //Reload slide
    $(window).resize(function(){
        swiper.update();
    });
    $(window).on('load', function () {
        swiper.update();
    });

    //comportamentos quando o slider inicia de trocar o slide
    swiper.on('slideChangeTransitionStart', function() {
        transition.removeClass('end');
        transition.addClass('start');
    });

    //comportamentos quando o slider termina de trocar o slide
    swiper.on('slideChangeTransitionEnd', function() {
        hash = window.location.hash.substring(1);
        //year.text(hash)
        $('body').removeAttr('class')
        $('body').addClass('theme-' + hash);
        page.removeClass('open');
        swiper.mousewheel.enable();
        
        transition.removeClass('start');
        transition.addClass('end');
    });

    Array.from(openBoxElements).forEach(function(openBox) {
        openBox.addEventListener('click', (event) => {
            const swiperElement = event.target.closest(".swiper-slide");
            const imageBoxElement = swiperElement.querySelector('.box');
            const pageElement = swiperElement.querySelector(".page");

            pageElement.classList.add('open');
            imageBoxElement.classList.toggle('up');

            swiper.mousewheel.disable();
            swiper.allowTouchMove = false;
        });
    });

    Array.from(closeElements).forEach(function(close) {
        close.addEventListener('click', (event) => {
            const swiperElement = event.target.closest(".swiper-slide");
            const imageBoxElement = swiperElement.querySelector('.box');
            const pageElement = swiperElement.querySelector(".page");

            pageElement.classList.remove('open');
            imageBoxElement.classList.toggle('up');

            swiper.mousewheel.enable();
            swiper.allowTouchMove = true;
        });
    });
});