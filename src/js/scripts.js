window.addEventListener("load", function(event) {

    let page = $('.page');
    let hash = window.location.hash ? window.location.hash.substring(1) : "2000";
    
    //troca a class do body para trocar o tema de acordo com o ano 
    $('body').toggleClass('theme-' + hash);     

    //monta slider de curtas dentro de page
    var curtas = [];
    $('.slide-curtas').each(function(index, element){

        $(this).addClass('s'+index);
        var sliderCurtas = new Swiper('.s'+index, {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              },
        });
        curtas.push(sliderCurtas);

    });
    
    //Deixa o slider de curtas flutuante no scroll
    page.each(function(){
        let selfPage = $(this);
        
        selfPage.scroll(function() {
            let selfWrapper = $(this).find('.wrapper-curtas'),
                curtasTop = selfWrapper.offset().top + 700,
                curtasW = $('.slide-curtas').width();
    
            selfWrapper.width(curtasW);
    
            if ( $(this).scrollTop() >= curtasTop ) {
                selfWrapper.addClass('float');
            }else{
                selfWrapper.removeClass('float');
            }
        });
    })

});
