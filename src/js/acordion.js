window.addEventListener("load", function(event) {
    $('.acordion__title').click(function(){
        let self = $(this),
            dad = self.closest('.acordion'),
            page = self.closest('.page');
    
        self.next('.acordion__text').slideDown();
        dad.siblings('.acordion').find('.acordion__text').slideUp();        
    })
    
    $('.acordion-close').click(function(){
        $(this).closest('.acordion__text').slideUp();
    })

});