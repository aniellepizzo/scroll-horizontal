const {src, dest, watch, series, parallel} = require('gulp');
const bs = require('browser-sync').create(),
      sass = require('gulp-sass'),
      babel = require('gulp-babel'),
      njx = require('gulp-nunjucks-render'),
      del = require('del'),
      fs = require('fs'),
      concat = require('gulp-concat');

const DEST_DIR = './public';
const SRC_DIR = './src';

// Local Server
const serve = () => {
    bs.init({
        server: {
          baseDir: DEST_DIR
        }
    });
  watch(`${SRC_DIR}/scss/**/**`, css);
  watch(`${SRC_DIR}/js/**/**`, parallel(js));
  watch(`${SRC_DIR}/img/**/**`, img);
  watch(`${SRC_DIR}/templates/**/**`, html);
};

const html = () =>
  src([
    `${SRC_DIR}/templates/*.html`
  ])
  .pipe( njx({
    path:`${SRC_DIR}/templates/`
  }))
  .pipe( dest(`${DEST_DIR}`) )
  .pipe( bs.stream());

  const css = () =>
  src([
    `${SRC_DIR}/scss/**.scss`
  ])
    .pipe( sass() )
    .pipe( dest(`${DEST_DIR}/css`) )
    .pipe( bs.stream() );

const js = () =>
    src([
      `node_modules/jquery/dist/jquery.min.js`,
      `node_modules/swiper/js/swiper.min.js`,
      `${SRC_DIR}/js/**.js`
    ])
    .pipe( babel({
      presets: ['@babel/preset-env']
    }) )
    .pipe(concat('scripts.js'))
    .pipe( dest(`${DEST_DIR}/js`) )
    .pipe( bs.stream() );


const cleanStatic = async () => await del(`${DEST_DIR}/**`);

const img = () =>
  src(`${SRC_DIR}/img/**/**`)
    .pipe( dest(`${DEST_DIR}/img`) );

const fonts = () =>
  src(`${SRC_DIR}/fonts/**/**`)
    .pipe( dest(`${DEST_DIR}/css/fonts`) );

const build = series(parallel( html, css, js, img, fonts), serve);
const staging = series(cleanStatic, parallel( html, css, js, img, fonts));
const production = series(parallel( html, css, js, img, fonts));

module.exports = {
  serve,
  css,
  js,
  img,
  fonts,
  html,
  cleanStatic,
  build,
  staging,
  production,
  default: build,
};
